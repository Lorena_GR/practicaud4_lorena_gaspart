package com.lorenagr.ropa_ceremonia.main;

import com.lorenagr.ropa_ceremonia.gui.Controlador;
import com.lorenagr.ropa_ceremonia.gui.Modelo;
import com.lorenagr.ropa_ceremonia.gui.Vista;

public class Main {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}