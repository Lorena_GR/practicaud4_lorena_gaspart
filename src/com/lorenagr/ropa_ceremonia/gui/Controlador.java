package com.lorenagr.ropa_ceremonia.gui;


import com.lorenagr.ropa_ceremonia.base.Coleccion;
import com.lorenagr.ropa_ceremonia.base.Diseniador;
import com.lorenagr.ropa_ceremonia.base.Prenda;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;

public class Controlador implements ActionListener, KeyListener, ListSelectionListener{

    public Vista vista;
    public Modelo modelo;

    /**
     * Constructor que inicia la vista, el modelo, conecta, lista los datos de la base y inicia los listeners
     * @param vista
     * @param modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;
        modelo.conectar();
        listarDiseniador(modelo.getDiseniador());
        listarColecciones(modelo.getColeccion());
        listarPrendas(modelo.getPrenda());
        addActionListeners(this);
        addKeyListeners(this);
        addLisSelectionListeners(this);
    }
    /**
     * Metodo que inicia los botones y les da un action command para el switch
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnAniadirDisen.addActionListener(listener);
        vista.btnAniadirDisen.setActionCommand("aniadirDisen");
        vista.btnModificarDisen.addActionListener(listener);
        vista.btnModificarDisen.setActionCommand("modificarDisen");
        vista.btnEliminarDisen.addActionListener(listener);
        vista.btnEliminarDisen.setActionCommand("eliminarDisen");
        vista.btnAniadirColeccion.addActionListener(listener);
        vista.btnAniadirColeccion.setActionCommand("aniadirColeccion");
        vista.btnModificarColeccion.addActionListener(listener);
        vista.btnModificarColeccion.setActionCommand("modificarColeccion");
        vista.btnEliminarColeccion.addActionListener(listener);
        vista.btnEliminarColeccion.setActionCommand("eliminarColeccion");
        vista.btnAniadirPrenda.addActionListener(listener);
        vista.btnAniadirPrenda.setActionCommand("aniadirPrenda");
        vista.btnModificarPrenda.addActionListener(listener);
        vista.btnModificarPrenda.setActionCommand("modificarPrenda");
        vista.btnEliminarPrenda.addActionListener(listener);
        vista.btnEliminarPrenda.setActionCommand("eliminarPrenda");
    }

    /**
     * Metodo que crea los listeners de la lista
     * @param listener
     */
    private void addLisSelectionListeners(ListSelectionListener listener){
        vista.listaDiseniadores.addListSelectionListener(listener);
        vista.listaColecciones.addListSelectionListener(listener);
        vista.listaPrendas.addListSelectionListener(listener);
    }

    /**
     * Metodo que inicia los key listeners para las busquedas
     * @param listener
     */
    private void addKeyListeners(KeyListener listener){
        vista.txtBuscarDisen.addKeyListener(listener);
        vista.txtBuscarColeccion.addKeyListener(listener);
        vista.txtBuscarPrenda.addKeyListener(listener);
    }

    /**
     * Metodo action performed con el switch que da las funcionalidades a los botones
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Diseniador disen;
        Coleccion col;
        Prenda prenda;
        switch (e.getActionCommand()){
            /**
             * Case para aniadir un diseniador
             */
            case "aniadirDisen":
                 disen = new Diseniador();
                modificarDisenFromCampos(disen);
                modelo.guardarDisen(disen);
                listarDiseniador(modelo.getDiseniador());
                break;
            /**
             * Case aniadir coleccion
             */
            case "aniadirColeccion":
                 col = new Coleccion();
                modificarColeccionFromCampos(col);
                modelo.guardarColeccion(col);
                listarColecciones(modelo.getColeccion());
                break;
            /**
             * Case aniadir prenda
             */
            case "aniadirPrenda":
                 prenda = new Prenda();
                modificarPrendaFromaCampos(prenda);
                modelo.guardarPrenda(prenda);
                listarPrendas(modelo.getPrenda());
                break;
            /**
             * case modificar diseniador
             */
            case "modificarDisen":
                disen = (Diseniador) vista.listaDiseniadores.getSelectedValue();
                modificarDisenFromCampos(disen);
                modelo.modificarDisen(disen);
                listarDiseniador(modelo.getDiseniador());
                break;
            /**
             * case modificar coleccion
             */
            case "modificarColeccion":
                col = (Coleccion) vista.listaColecciones.getSelectedValue();
                modificarColeccionFromCampos(col);
                modelo.modificarColeccion(col);
                listarColecciones(modelo.getColeccion());
                break;
            /**
             * case modificar prenda
             */
            case "modificarPrenda":
                prenda = (Prenda) vista.listaPrendas.getSelectedValue();
                modificarPrendaFromaCampos(prenda);
                modelo.modificarPrenda(prenda);
                listarPrendas(modelo.getPrenda());
                break;
            /**
             * case eliminar prenda
             */
            case "eliminarDisen":
                disen = (Diseniador) vista.listaDiseniadores.getSelectedValue();
                modelo.eliminarDisen(disen);
                listarDiseniador(modelo.getDiseniador());
                break;
            /**
             * case eliminar coleccion
             */
            case "eliminarColeccion":
                col = (Coleccion) vista.listaColecciones.getSelectedValue();
                modelo.eliminarColeccion(col);
                listarColecciones(modelo.getColeccion());
                break;
            /**
             * case eliminar prenda
             */
            case "eliminarPrenda":
                prenda = (Prenda) vista.listaPrendas.getSelectedValue();
                modelo.eliminarPrenda(prenda);
                listarPrendas(modelo.getPrenda());
                break;

        }
    }

    /**
     * listar diseniador
     * @param list
     */
    private void listarDiseniador(List<Diseniador> list){
        vista.dlmDiseniador.clear();
        for (Diseniador disen : list){
            vista.dlmDiseniador.addElement(disen);
        }
    }

    /**
     * listar coleccion
     * @param list
     */
    private void listarColecciones(List<Coleccion> list){
        vista.dlmColeccion.clear();
        for (Coleccion col : list){
            vista.dlmColeccion.addElement(col);
        }
    }

    /**
     * listar prenda
     * @param list
     */
    private void listarPrendas(List<Prenda> list){
        vista.dlmPrenda.clear();
        for(Prenda prenda : list){
            vista.dlmPrenda.addElement(prenda);
        }
    }

    /**
     * metodo que agarra los textos de los campos de diseñador
     * @param disen
     */
    private void modificarDisenFromCampos(Diseniador disen){
        disen.setNombre(vista.txtNombreDisen.getText());
        disen.setApellidos(vista.txtApellidosDisen.getText());
        disen.setFecha_nacimiento(vista.dateFechaNacDisen.getDate());
        disen.setGenero(String.valueOf(vista.cbGeneroDisen.getSelectedItem()));
    }

    /**
     * metodo que agarra los textos de los campos de coleccion
     * @param col
     */
    private void modificarColeccionFromCampos(Coleccion col){
        col.setNombre(vista.txtNombreColeccion.getText());
        col.setMarca(String.valueOf(vista.cbMarcaColeccion.getSelectedItem()));
        col.setEstacion(String.valueOf(vista.cbEstacionColeccion.getSelectedItem()));
        col.setFecha_creacion(vista.dateFechaCreacionColeccion.getDate());
    }

    /**
     * metodo que agarra los textos de los campos de prenda
     * @param prenda
     */
    private void modificarPrendaFromaCampos(Prenda prenda){
        prenda.setNombre(vista.txtNombrePrenda.getText());
        prenda.setTipo(String.valueOf(vista.cbTipoPrenda.getSelectedItem()));
        prenda.setColor(String.valueOf(vista.cbColorPrenda.getSelectedItem()));
        prenda.setTalla(vista.sliderTallaPrenda.getValue());
        prenda.setPrecio(Double.parseDouble(vista.txtPrecioPrenda.getText()));
        prenda.setFecha_compra(vista.dateFechaCompraPrenda.getDate());
        prenda.setFecha_entrega(vista.dateFechaEntregaPrenda.getDate());

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /**
     * Metodo que pinta los diseñadores, colecciones o prendas que coincida con el texto de buscar
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if(e.getSource() == vista.txtBuscarDisen){
            listarDiseniador(modelo.getDiseniador(vista.txtBuscarDisen.getText()));
        } else if(e.getSource() == vista.txtBuscarColeccion){
            listarColecciones(modelo.getColeccion(vista.txtBuscarColeccion.getText()));
        } else if(e.getSource() == vista.txtBuscarPrenda){
            listarPrendas(modelo.getPrenda(vista.txtBuscarPrenda.getText()));
        }
    }

    /**
     * Metodo que pinta los campos en sus respectivos campos de un elemento seleccionado de la lista
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == vista.listaDiseniadores){
                Diseniador seleccionDisen = (Diseniador) vista.listaDiseniadores.getSelectedValue();
                vista.txtNombreDisen.setText(seleccionDisen.getNombre());
                vista.txtApellidosDisen.setText(seleccionDisen.getApellidos());
                vista.dateFechaNacDisen.setText(String.valueOf(seleccionDisen.getFecha_nacimiento()));
                vista.cbGeneroDisen.setSelectedItem(seleccionDisen.getGenero());
            }else if(e.getSource() == vista.listaColecciones){
                Coleccion seleccionColeccion = (Coleccion) vista.listaColecciones.getSelectedValue();
                vista.txtNombreColeccion.setText(seleccionColeccion.getNombre());
                vista.cbMarcaColeccion.setSelectedItem(seleccionColeccion.getMarca());
                vista.cbEstacionColeccion.setSelectedItem(seleccionColeccion.getEstacion());
                vista.dateFechaCreacionColeccion.setText(String.valueOf(seleccionColeccion.getFecha_creacion()));
            }else if(e.getSource() == vista.listaPrendas){
                Prenda seleccionPrenda = (Prenda)vista.listaPrendas.getSelectedValue();
                vista.txtNombrePrenda.setText(seleccionPrenda.getNombre());
                vista.cbTipoPrenda.setSelectedItem(seleccionPrenda.getTipo());
                vista.cbColorPrenda.setSelectedItem(seleccionPrenda.getColor());
                vista.sliderTallaPrenda.setValue(seleccionPrenda.getTalla());
                vista.txtPrecioPrenda.setText(String.valueOf(seleccionPrenda.getPrecio()));
                vista.dateFechaCompraPrenda.setText(String.valueOf(seleccionPrenda.getFecha_compra()));
                vista.dateFechaEntregaPrenda.setText(String.valueOf(seleccionPrenda.getFecha_entrega()));
            }
            }
        }
    }