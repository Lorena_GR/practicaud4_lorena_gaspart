package com.lorenagr.ropa_ceremonia.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.lorenagr.ropa_ceremonia.base.Coleccion;
import com.lorenagr.ropa_ceremonia.base.Diseniador;
import com.lorenagr.ropa_ceremonia.base.Prenda;
import com.lorenagr.ropa_ceremonia.enums.*;
import com.lorenagr.ropa_ceremonia.enums.Color;

import javax.swing.*;
import java.awt.*;

public class Vista extends JFrame {
    private final static String TITULOFRAME = "Aplicación ropa de ceremonia hibernate";

    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JPanel panelDisen;
    private JPanel panelColeccion;
    private JPanel panelPrenda;

    JTextField txtNombreDisen;
     JTextField txtApellidosDisen;
     JComboBox cbGeneroDisen;
     JButton btnAniadirDisen;
     JButton btnModificarDisen;
     JButton btnEliminarDisen;
     JList listaDiseniadores;
    DatePicker dateFechaNacDisen;

     JTextField txtNombreColeccion;
     JComboBox cbMarcaColeccion;
     JComboBox cbEstacionColeccion;
     JButton btnAniadirColeccion;
     JButton btnModificarColeccion;
     JButton btnEliminarColeccion;
    JList listaColecciones;
     DatePicker dateFechaCreacionColeccion;


    JTextField txtNombrePrenda;
     JComboBox cbTipoPrenda;
     JComboBox cbColorPrenda;
     JSlider sliderTallaPrenda;
     JTextField txtPrecioPrenda;
    JButton btnAniadirPrenda;
     JButton btnModificarPrenda;
     JButton btnEliminarPrenda;
     JList listaPrendas;
     DatePicker dateFechaCompraPrenda;
     DatePicker dateFechaEntregaPrenda;
     JTextField txtBuscarColeccion;
     JTextField txtBuscarDisen;
     JTextField txtBuscarPrenda;

    // DEFAULT LIST MODEL
    DefaultListModel <Coleccion> dlmColeccion;
    DefaultListModel <Diseniador> dlmDiseniador;
    DefaultListModel <Prenda> dlmPrenda;

    public Vista(){
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Metodo que hace la vista
     */
    public void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+500, this.getHeight()+100));
        this.setLocationRelativeTo(this);
        setEnumComboBox();
        setTableModels();
    }

    /**
     * Metodo que inicia y da modelo a los defaultlistmodel
     */
     void setTableModels() {
         dlmDiseniador = new DefaultListModel();
         listaDiseniadores.setModel(dlmDiseniador);
         dlmColeccion = new DefaultListModel();
         listaColecciones.setModel(dlmColeccion);
         dlmPrenda = new DefaultListModel();
         listaPrendas.setModel(dlmPrenda);
    }

    /**
     * Metodo que coge los elementos de un enum y los mete en un combobox
     */
     void setEnumComboBox(){
        for(Color constant : Color.values()){
            cbColorPrenda.addItem(constant.getColor());
        }
        for(Estacion constant : Estacion.values()){
            cbEstacionColeccion.addItem(constant.getEstacion());
        }
        for(Marca constant : Marca.values()){
            cbMarcaColeccion.addItem(constant.getMarca());
        }
        for(Genero constant : Genero.values()){
            cbGeneroDisen.addItem(constant.getGenero());
        }
        for(Tipo constant : Tipo.values()){
            cbTipoPrenda.addItem(constant.getTipo());
        }
    }
}

