package com.lorenagr.ropa_ceremonia.gui;


import com.lorenagr.ropa_ceremonia.base.Coleccion;
import com.lorenagr.ropa_ceremonia.base.Diseniador;
import com.lorenagr.ropa_ceremonia.base.Prenda;
import com.lorenagr.ropa_ceremonia.util.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Modelo {
    /**
     * CREAMOS LAS VARIABLES, COLECCIONES Y CLIENTES
     */
    String DATABASE = "ropa_ceremonia";
    String COLECCION_DISEN = "diseniador";
    String COLECCION_COLECCION = "coleccion";
    String COLECCION_PRENDA = "prenda";

    private MongoClient cliente;
    private MongoCollection<Document> diseniadores;
    private MongoCollection<Document> colecciones;
    private MongoCollection<Document> prendas;

    /**
     * Metodo conectar que inicia el cliente, crea la base de datos y las colecciones
     */
    public void conectar(){

        cliente = new MongoClient();
        MongoDatabase db = cliente.getDatabase(DATABASE);
        diseniadores = db.getCollection(COLECCION_DISEN);
        colecciones = db.getCollection(COLECCION_COLECCION);
        prendas = db.getCollection(COLECCION_PRENDA);
    }

    /**
     * Metodo desconectar
     */
    public void desconectar(){
        cliente.close();
    }

    /**
     * Metodo que returna la lista de diseniadores
     * @return
     */
    public List<Diseniador> getDiseniador(){
        ArrayList<Diseniador> lista = new ArrayList<>();
        Iterator<Document> it = diseniadores.find().iterator();
        while (it.hasNext()){
            lista.add(documentToDiseniador(it.next()));
        }
        return lista;
    }

    /**
     * Metodo que lista los diseniadores comparandolos con los criterios
     * @param txt
     * @return
     */
    public List<Diseniador> getDiseniador(String txt){
        ArrayList<Diseniador> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombre"
                , new Document("$regex","/*"+txt+"/*")));
        listaCriterios.add(new Document("apellidos"
                , new Document("$regex","/*"+txt+"/*")));
        query.append("$or", listaCriterios);
        Iterator<Document> iterator = diseniadores.find(query).iterator();
        while(iterator.hasNext()){
            lista.add(documentToDiseniador(iterator.next()));
        }
        return lista;

    }

    /**
     * Metodo que retorna la lista de colecciones
     * @return
     */
    public List<Coleccion> getColeccion(){
        ArrayList<Coleccion> lista = new ArrayList<>();
        Iterator<Document> it = colecciones.find().iterator();
        while (it.hasNext()){
            lista.add(documentToColeccion(it.next()));
        }
        return lista;
    }


    /**
     * Metodo que retorna la lista de colecciones comparada con unos criterios
     * @param txt
     * @return
     */
    public List<Coleccion> getColeccion(String txt){
        ArrayList<Coleccion> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombre"
                , new Document("$regex","/*"+txt+"/*")));
        listaCriterios.add(new Document("marca"
                , new Document("$regex","/*"+txt+"/*")));
        query.append("$or", listaCriterios);
        Iterator<Document> iterator = colecciones.find(query).iterator();
        while(iterator.hasNext()){
            lista.add(documentToColeccion(iterator.next()));
        }
        return lista;

    }

    /**
     * Metodo que retorna la lista de prendas
     * @return
     */
    public List<Prenda> getPrenda(){
        ArrayList<Prenda> lista = new ArrayList<>();
        Iterator<Document> it = prendas.find().iterator();
        while (it.hasNext()){
            lista.add(documentToPrenda(it.next()));
        }
        return lista;
    }

    /**
     * Metodo que retorna la lista de prendas, comparandola con unos criterios
     * @param txt
     * @return
     */
    public List<Prenda> getPrenda(String txt){
        ArrayList<Prenda> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();
        listaCriterios.add(new Document("nombre"
                , new Document("$regex","/*"+txt+"/*")));
        listaCriterios.add(new Document("precio"
                , new Document("$regex","/*"+txt+"/*")));
        query.append("$or", listaCriterios);
        Iterator<Document> iterator = prendas.find(query).iterator();
        while(iterator.hasNext()){
            lista.add(documentToPrenda(iterator.next()));
        }
        return lista;

    }

    /**
     * Metodo para insertar un diseniador
     * @param unDisen
     */
    public void guardarDisen(Diseniador unDisen){
        diseniadores.insertOne(diseniadorToDocument(unDisen));
    }

    /**
     * Metodo para insertar una coleccion
     * @param unaCol
     */
    public void guardarColeccion(Coleccion unaCol){
        colecciones.insertOne(coleccionToDocument(unaCol));
    }

    /**
     * Metodo que inserta una prenda
     * @param unaPrenda
     */
    public void guardarPrenda(Prenda unaPrenda){
        prendas.insertOne(prendaToDocument(unaPrenda));
    }

    /**
     * Metodo que modifica un diseniador
     * @param unDisen
     */
    public void modificarDisen(Diseniador unDisen){
        diseniadores.replaceOne(new Document("_id", unDisen.getId()),
                diseniadorToDocument(unDisen));
    }

    /**
     * Metodo que modifica una coleccion
     * @param unaCol
     */
    public void modificarColeccion(Coleccion unaCol){
        colecciones.replaceOne(new Document("_id", unaCol.getId()),
                coleccionToDocument(unaCol));
    }

    /**
     * Metodo que modifica una prenda
     * @param unaPrenda
     */
    public void modificarPrenda(Prenda unaPrenda){
        prendas.replaceOne(new Document("_id", unaPrenda.getId()),
                prendaToDocument(unaPrenda));
    }

    /**
     * Metodo que eliminar un diseniador
     * @param unDisen
     */
    public void eliminarDisen(Diseniador unDisen){
        diseniadores.deleteOne(diseniadorToDocument(unDisen));
    }

    /**
     * Metodo que elimina una coleccion
     * @param unaCol
     */
    public void eliminarColeccion(Coleccion unaCol){
        colecciones.deleteOne(coleccionToDocument(unaCol));
    }

    /**
     * Metodo que elimina una prenda
     * @param unaPrenda
     */
    public void eliminarPrenda(Prenda unaPrenda){
        prendas.deleteOne(prendaToDocument(unaPrenda));
    }

    /**
     * Metodo que pasar un documento a un diseniador
     * @param dc
     * @return
     */
    public Diseniador documentToDiseniador(Document dc){
        Diseniador disen = new Diseniador();
        disen.setId(dc.getObjectId("_id"));
        disen.setNombre(dc.getString("nombre"));
        disen.setApellidos(dc.getString("apellidos"));
        disen.setFecha_nacimiento(Util.parsearFecha(dc.getString("fecha_nacimiento")));
        disen.setGenero(dc.getString("genero"));
        return disen;
    }

    /**
     * Metodo que pasa un documento a una coleccion
     * @param dc
     * @return
     */
    public Coleccion documentToColeccion(Document dc){
        Coleccion coleccion = new Coleccion();
        coleccion.setId(dc.getObjectId("_id"));
        coleccion.setNombre(dc.getString("nombre"));
        coleccion.setMarca(dc.getString("marca"));
        coleccion.setEstacion(dc.getString("estacion"));
        coleccion.setFecha_creacion(Util.parsearFecha(dc.getString("fecha_creacion")));
        return coleccion;
    }

    /**
     * Metodo que pasa un documento a una prenda
     * @param dc
     * @return
     */
    public Prenda documentToPrenda(Document dc){
        Prenda prenda = new Prenda();
        prenda.setId(dc.getObjectId("_id"));
        prenda.setNombre(dc.getString("nombre"));
        prenda.setTipo(dc.getString("tipo"));
        prenda.setColor(dc.getString("color"));
        prenda.setTalla(dc.getInteger("talla"));
        prenda.setPrecio(dc.getDouble("precio"));
        prenda.setFecha_compra(Util.parsearFecha(dc.getString("fecha_compra")));
        prenda.setFecha_entrega(Util.parsearFecha(dc.getString("fecha_entrega")));
        return prenda;
    }

    /**
     * Metodo que pasa un diseniador a un documento
     * @param unDisen
     * @return
     */
    public Document diseniadorToDocument(Diseniador unDisen){
        Document dc = new Document();
        dc.append("nombre", unDisen.getNombre());
        dc.append("apellidos", unDisen.getApellidos());
        dc.append("fecha_nacimiento", Util.formatearFecha(unDisen.getFecha_nacimiento()));
        dc.append("genero", unDisen.getGenero());
        return dc;
    }

    /**
     * Metodo que pasa una coleccion a documento
     * @param unaCol
     * @return
     */
    public Document coleccionToDocument(Coleccion unaCol){
        Document dc = new Document();
        dc.append("nombre", unaCol.getNombre());
        dc.append("marca", unaCol.getMarca());
        dc.append("estacion", unaCol.getEstacion());
        dc.append("fecha_creacion", Util.formatearFecha(unaCol.getFecha_creacion()));
        return dc;
    }

    /**
     * Metodo que pasa una prenda a un documento
     * @param unaPrenda
     * @return
     */
    public Document prendaToDocument(Prenda unaPrenda){
        Document dc = new Document();
        dc.append("nombre", unaPrenda.getNombre());
        dc.append("tipo", unaPrenda.getTipo());
        dc.append("color", unaPrenda.getColor());
        dc.append("talla", unaPrenda.getTalla());
        dc.append("precio", unaPrenda.getPrecio());
        dc.append("fecha_compra", Util.formatearFecha(unaPrenda.getFecha_compra()));
        dc.append("fecha_entrega", Util.formatearFecha(unaPrenda.getFecha_entrega()));
        return dc;
    }

}