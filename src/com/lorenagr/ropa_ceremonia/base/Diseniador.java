package com.lorenagr.ropa_ceremonia.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Diseniador {
    /**
     * Clase que crea los campos de un diseniador, getters y setters, constructores vacios y llenos y el to String
     */
    private ObjectId id;
    private String nombre;
    private String apellidos;
    private LocalDate fecha_nacimiento;
    private String genero;

    public Diseniador(String nombre, String apellidos,LocalDate fecha_nacimiento, String genero) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.fecha_nacimiento = fecha_nacimiento;
        this.genero = genero;
    }

    public Diseniador() {
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public LocalDate getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(LocalDate fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    @Override
    public String toString() {
        return "Diseniador{" +
                "nombre='" + nombre + '\'' +
                ", apellidos='" + apellidos + '\'' +
                ", fecha_nacimiento=" + fecha_nacimiento +
                ", genero='" + genero + '\'' +
                '}';
    }
}
