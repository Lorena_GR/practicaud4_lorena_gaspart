package com.lorenagr.ropa_ceremonia.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Prenda {
    /**
     * Clase que crea los campos de una prenda, getters y setters, constructores vacios y llenos y el to String
     */
    private ObjectId id;
    private String nombre;
    private String tipo;
    private String color;
    private int talla;
    private double precio;
    private LocalDate fecha_compra;
    private LocalDate fecha_entrega;

    public Prenda(String nombre, String tipo, String color, int talla, float precio, LocalDate fecha_compra, LocalDate fecha_entrega) {
        this.nombre = nombre;
        this.tipo = tipo;
        this.color = color;
        this.talla = talla;
        this.precio = precio;
        this.fecha_compra = fecha_compra;
        this.fecha_entrega = fecha_entrega;
    }

    public Prenda() {
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getTalla() {
        return talla;
    }

    public void setTalla(int talla) {
        this.talla = talla;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public LocalDate getFecha_compra() {
        return fecha_compra;
    }

    public void setFecha_compra(LocalDate fecha_compra) {
        this.fecha_compra = fecha_compra;
    }

    public LocalDate getFecha_entrega() {
        return fecha_entrega;
    }

    public void setFecha_entrega(LocalDate fecha_entrega) {
        this.fecha_entrega = fecha_entrega;
    }

    @Override
    public String toString() {
        return "Prenda{" +
                "nombre='" + nombre + '\'' +
                ", tipo='" + tipo + '\'' +
                ", color='" + color + '\'' +
                ", talla=" + talla +
                ", precio=" + precio +
                ", fecha_compra=" + fecha_compra +
                ", fecha_entrega=" + fecha_entrega +
                '}';
    }
}
