package com.lorenagr.ropa_ceremonia.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

public class Coleccion {
    /**
     * Clase que crea los campos de una coleccion, getters y setters, constructores vacios y llenos y el to String
     */
    private ObjectId id;
    private String nombre;
    private String marca;
    private String estacion;
    private LocalDate fecha_creacion;

    public Coleccion(String nombre, String marca, String estacion, LocalDate fecha_creacion) {
        this.nombre = nombre;
        this.marca = marca;
        this.estacion = estacion;
        this.fecha_creacion = fecha_creacion;
    }

    public Coleccion(){

    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getEstacion() {
        return estacion;
    }

    public void setEstacion(String estacion) {
        this.estacion = estacion;
    }

    public LocalDate getFecha_creacion() {
        return fecha_creacion;
    }

    public void setFecha_creacion(LocalDate fecha_creacion) {
        this.fecha_creacion = fecha_creacion;
    }

    @Override
    public String toString() {
        return "Coleccion{" +
                "nombre='" + nombre + '\'' +
                ", marca='" + marca + '\'' +
                ", estacion='" + estacion + '\'' +
                ", fecha_creacion=" + fecha_creacion +
                '}';
    }
}
