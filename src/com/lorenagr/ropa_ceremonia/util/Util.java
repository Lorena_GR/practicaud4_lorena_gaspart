package com.lorenagr.ropa_ceremonia.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;


public class Util {
    /**
     * Retorna una fecha formateada
     * @param fechaMatriculacion
     * @return
     */
    public static String formatearFecha(LocalDate fechaMatriculacion) {
        DateTimeFormatter formateador =DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return formateador.format(fechaMatriculacion);
    }

    /**
     * Retorna una fecha parseada
     * @param fecha
     * @return
     */
    public static LocalDate parsearFecha(String fecha) {
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return LocalDate.parse(fecha,formateador);
    }
}
