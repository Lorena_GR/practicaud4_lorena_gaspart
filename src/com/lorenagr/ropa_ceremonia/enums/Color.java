package com.lorenagr.ropa_ceremonia.enums;

public enum Color {
    /**
     * ENUM DE COLOR DE PRENDA
     */
    NEGRO("Negro"),
    AZUL("Azul"),
    MARRON("Marrón"),
    GRIS("Gris"),
    VERDE("Verde"),
    NARANJA("Naranja"),
    ROSA("Rosa"),
    PURPURA("Púrpura"),
    ROJO("Rojo"),
    BLANCO("Blanco"),
    AMARILLO("Amarillo");
    private String color;

    /**
     * Constructor de la clase enum de color. Que inicia el atributo color.
     * @param color
     */
    Color(String color){
        this.color=color;
    }

    public String getColor() {
        return color;
    }
}
