package com.lorenagr.ropa_ceremonia.enums;

public enum Tipo {
    /**
     * ENUM DEL TIPO DE PRENDA
     */
    VESTIDO("Vestido"),
    TRAJE("Traje");

    String tipo;

    Tipo(String tipo){
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

}
